# Terraform Example

### Directory structure

* `.gitlab` - local templates
  * `Terraform.gitlab-ci.yml` - template for a Terraform pipeline
* `.gitlab-ci.yml` - main CI configuration, includes `demo/development` and `demo/production`
* `demo` - project name within repository (eg. `dns`, `servers`, etc.)
  * `development` - environment within a project containing `*.tf` files for `demo/development`
    * `.gitlab-ci.yml` - CI configuration for `demo/development`
  * `production` - environment within a project containing `*.tf` files for `demo/production`
    * `.gitlab-ci.yml` - CI configuration for `demo/production`
